//
//  TrackPadViewController.m
//  Airmote+
//
//  Created by Long Nguyen on 11/6/13.
//  Copyright (c) 2013 Long Nguyen. All rights reserved.
//

#import "TrackPadViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ProtoHelper.h"
#import "TrackPadView.h"
#import "JDStatusBarNotification+Extension.h"
#import "NSString+IPAddress.h"

#define kIPAddressAlertTitle @"Manual Connect"
#define kSetupWiFiAlertTitle @"Setup Wifi"

@implementation TrackPadViewController {
    Event *_oauthEvent;
    __weak IBOutlet UIView *inputView;
    __weak IBOutlet NSLayoutConstraint *inputViewTopConstrain;
    __weak IBOutlet UITextView *plainText;
    __weak IBOutlet NSLayoutConstraint *bottomControlsConstrain;
    __weak IBOutlet UIPageControl *pageControl;
    NSString *lastManuallyConnectedIP;
    BOOL userDidStartScanning;
    BOOL isFirstLaunch;
}

static const uint8_t kMotionShakeTag = 6;

@synthesize ipAlertView = _ipAlertView;
@synthesize trackpadView = _trackpadView;
@synthesize setupController = _setupController;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        isFirstLaunch = YES;

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[IAConnection sharedConnection] setDelegate:self];

    [self.navigationController setNavigationBarHidden:YES];
    _trackpadView.viewController = self;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishWifiSetup:)
                                                 name:kInAirDeviceDidConnectToWifiNotification
                                               object:nil];

    bottomControlsConstrain.constant = -54;

    [JDStatusBarNotification inAirInit];
}

- (void)appBecomeActive:(NSNotification *)notification {

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (![[IAConnection sharedConnection] isConnected] && ![[IAConnection sharedConnection] isProcessing] && ![_actionSheet isVisible]) {
            if (isFirstLaunch) {
                isFirstLaunch = NO;
                [self forceStartConnection];
            } else {
                if (![[IAConnection sharedConnection] reconnectToLastConnectedService]) {
                    [self forceStartConnection];
                }
            }
        }
    });

}


- (void)startWifiSetup {
    _setupController.dataSource = nil;
    _setupController = [[WifiSetupController alloc] init];
    _setupController.dataSource = _setupController;
    [self presentViewController:_setupController animated:YES completion:nil];
}


- (BOOL)shouldConnectAutomatically {
    return YES;
}

- (void)didStartScanning {
//  [SVProgressHUD showWithStatus:@"Scanning..."];
    [JDStatusBarNotification showWithStatus:@"Scanning..."];
    [JDStatusBarNotification showActivityIndicator:YES indicatorStyle:UIActivityIndicatorViewStyleGray];
}

- (void)didStartConnecting {
//  [SVProgressHUD showWithStatus:@"Connecting..."];
    [JDStatusBarNotification showWithStatus:@"Connecting..."];
    [JDStatusBarNotification showActivityIndicator:YES indicatorStyle:UIActivityIndicatorViewStyleGray];
}


- (void)didConnect:(NSString *)hostName {
    NSString *message = [NSString stringWithFormat:@"Connected to %@", hostName];
//  [SVProgressHUD showSuccessWithStatus:message];
    [JDStatusBarNotification showSuccessWithStatus:message dismissAfter:kAnimationSlow];
}


- (void)didFoundServices:(NSArray *)foundServices {
    if (userDidStartScanning) {
        userDidStartScanning = NO;
        if (foundServices.count > 0) {
            [JDStatusBarNotification dismiss];
            [self showActionSheetForServices:foundServices];
        } else {
            [JDStatusBarNotification showErrorWithStatus:@"No InAiR devices found"];
        }
    }
}


- (void)didFailToConnect:(NSError *)error {
    switch (error.code) {
        case IAConnectionErrorServicesNotFound:
        case IAConnectionErrorDidNotSearch:
        case IAConnectionErrorDiscoveryTimedOut:
            [self showActionSheetForServices:nil];

        case IAConnectionErrorServiceNotResolved:
        case IAConnectionErrorSocketInvalidData:
        case IAConnectionErrorWifiNotAvailable:
        case IAConnectionErrorFailToConnectSocket:
            [JDStatusBarNotification showErrorWithStatus:[error localizedFailureReason]];

            break;

        case IAConnectionErrorSocketDisconnected:
            [JDStatusBarNotification showErrorWithStatus:[error localizedDescription]];
            break;

        case IAConnectionErrorFailToSendEvent:
            NSLog(@"Failed to send event");
            break;

        default:
            DDLogError(@"ERROR: %@", [error localizedFailureReason]);
            break;
    }
}


- (void)didReceiveEvent:(Event *)event {
    NSLog(@"%@", event);

    switch (event.type) {
        case EventTypeOauthRequest:
            [self processOAuthRequest:event];
            break;

        case EventTypeTextInputRequest:
            [self showInputView];
            break;

        case EventTypeWebviewRequest:
            [self processWebViewRequest:event];
            break;
        default:
            break;
    }

}

- (void)didStartUSBConnection {
    [JDStatusBarNotification showUSBConnection];

    // Delay the pop up of setup wifi alert box for 0.1 seconds so that if setup wifi interface already show up
    // we will skip showing the alert box.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.presentedViewController == nil || self.presentedViewController != _setupController) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:kSetupWiFiAlertTitle message:@"Do you want to setup WiFi connection for your InAiR?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            [alertView show];
        }
    });
}

- (void)didStopUSBConnection:(NSError *)error {
    [JDStatusBarNotification dismissAfter:kAnimationFast];
    if (_setupController != nil) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [IAConnection sharedConnection].delegate = self;
//  [self didStartUSBConnection];
}

- (void)showInputView {
    plainText.text = @"";
    [plainText becomeFirstResponder];
}

#pragma mark - Action sheets


- (void)showActionSheetForServices:(NSArray *)services {

    if ([_actionSheet isVisible]) {
        return;
    }

    _actionSheet = [[UIActionSheet alloc] init];
    [_actionSheet setTitle:@"Choose a device"];
    [_actionSheet setDelegate:self];

    for (NSNetService *service in services) {
        NSString *title = service.name;
        [_actionSheet addButtonWithTitle:title];
    }

    [_actionSheet addButtonWithTitle:@"Other"];

    [_actionSheet addButtonWithTitle:@"Cancel"];
    _actionSheet.cancelButtonIndex = services.count + 1;

    [_actionSheet showInView:self.view];
}

#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        if (buttonIndex == actionSheet.numberOfButtons - 2) {
            _ipAlertView = [[UIAlertView alloc] initWithTitle:kIPAddressAlertTitle
                                                      message:@"Please enter your InAir box IP Address."
                                                     delegate:self
                                            cancelButtonTitle:@"Done"
                                            otherButtonTitles:nil];
            _ipAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *textField = [_ipAlertView textFieldAtIndex:0];
            textField.placeholder = @"192.168.1.1";
            textField.returnKeyType = UIReturnKeyGo;
            textField.delegate = self;
            [_ipAlertView show];
        } else {
            [[IAConnection sharedConnection] connectToServiceAtIndex:(NSUInteger) buttonIndex];
        }
    } else {
        if (![[IAConnection sharedConnection] isConnected]) {
            [JDStatusBarNotification showErrorWithStatus:kConnectionNotAvailable];
        }

    }
}

#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([alertView.title isEqualToString:@"OAuth"]) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [self processOAuthRequest];
        }
        _oauthEvent = nil;
    } else if ([alertView.title isEqualToString:kIPAddressAlertTitle]) {

        NSString *ipaddress = [alertView textFieldAtIndex:0].text;
        if ([ipaddress isKindOfClass:[NSString class]] && [ipaddress isValidIPAddress]) {
            [[IAConnection sharedConnection] stop];
            [[IAConnection sharedConnection] resetStates];
            [[IAConnection sharedConnection] connectToHost:ipaddress];
        } else {
            [JDStatusBarNotification showErrorWithStatus:@"Invalid IP address" dismissAfter:2.0];
        }
//    }
    } else if ([alertView.title isEqualToString:kSetupWiFiAlertTitle]) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            if (self.presentedViewController != nil) {
                if ([self.actionSheet isVisible]) {
                    [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:YES];
                }
                [self dismissViewControllerAnimated:YES completion:^{
                    [self startWifiSetup];
                }];

            } else {
                [self startWifiSetup];
            }

        }
    }

}

#pragma mark -
#pragma mark Motion

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        Event *ev = [ProtoHelper motionEventWithTimestamp:(SInt64) (event.timestamp * 1000)
                                                     type:MotionEventTypeShake];
        [[IAConnection sharedConnection] sendEvent:ev withTag:kMotionShakeTag];
    }
}


- (void)showWebViewWithURL:(NSURL *)url oauthEvent:(Event *)oauthEvent webEvent:(Event *)webEvent {
    self.webViewController = [[WebViewController alloc] initWithUrl:url];
    self.webViewController.oauthEvent = oauthEvent;
    self.webViewController.webViewEvent = webEvent;
    [self.webViewController showFromController:self];
}

#pragma mark -
#pragma mark WebView


- (void)processWebViewRequest:(Event *)ev {
    _oauthEvent = nil;

    WebViewRequestEvent *event = [ev getExtension:[WebViewRequestEvent event]];

    if (event.url != nil) {
        if ([event.url rangeOfString:@"https://www.inair.tv/setupwifi"].location != NSNotFound) {
            [self settingsButtonTapped:nil];
        } else if ([event.url rangeOfString:@"http"].location != NSNotFound) {
            [self showWebViewWithURL:[NSURL URLWithString:event.url] oauthEvent:nil webEvent:ev];
        } else {
            NSLog(@"URL [%@] using a non-supported protocol", event.url);
        }
    }
}

#pragma mark -
#pragma mark OAuth

- (void)processOAuthRequest:(Event *)event {
    _oauthEvent = event;

    [self processOAuthRequest];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"OAuth" message:@"InAir would like to open webview for OAuth authentication." delegate:self cancelButtonTitle:@"Don't Allow" otherButtonTitles:@"OK", nil];
//    [alertView show];
}

- (void)processOAuthRequest {
    if (_oauthEvent == nil) {
        return;
    }

    OAuthRequestEvent *event = [_oauthEvent getExtension:[OAuthRequestEvent event]];
    [self showWebViewWithURL:[NSURL URLWithString:event.authUrl] oauthEvent:_oauthEvent webEvent:nil];
}

#pragma mark - Privates



//- (WebViewController *)webViewController {
//  if (_webViewController == nil) {
//    _webViewController = [[WebViewController alloc] init];
//  }
//
//  return _webViewController;
//}

#pragma mark - DidFinishWifiSetup

- (void)didFinishWifiSetup:(id)didFinishWifiSetup {
    [[IAConnection sharedConnection] setDelegate:self];
}


- (void)didStopConnection {
    [SVProgressHUD dismiss];
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if ([_ipAlertView textFieldAtIndex:0] == textField) {
        NSString *ipaddress = [_ipAlertView textFieldAtIndex:0].text;
        [_ipAlertView dismissWithClickedButtonIndex:0 animated:true];
        [[IAConnection sharedConnection] stop];
        [[IAConnection sharedConnection] resetStates];
        [[IAConnection sharedConnection] connectToHost:ipaddress];
    }
    return YES;
}


#pragma mark - Show/Hide Keyboard


- (void)keyboardWillHide:(NSNotification *)notification {
    if (!plainText.isFirstResponder)
        return;

    NSDictionary *userInfo = [notification userInfo];
    float duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = (UIViewAnimationCurve) [userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    UIViewAnimationOptions curveOption = (UIViewAnimationOptions) (curve << 16);
    [UIView animateWithDuration:duration
                          delay:0
                        options:curveOption
                     animations:^{
                         inputViewTopConstrain.constant = -inputView.frame.size.height - 20;
                         [[self view] layoutIfNeeded];

                     } completion:NULL];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    if (!plainText.isFirstResponder)
        return;

    NSDictionary *userInfo = [notification userInfo];
    float duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = (UIViewAnimationCurve) [userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    UIViewAnimationOptions curveOption = (UIViewAnimationOptions) (curve << 16);
    [UIView animateWithDuration:duration
                          delay:0
                        options:curveOption
                     animations:^{
                         inputViewTopConstrain.constant = 40;
                         [[self view] layoutIfNeeded];
                     } completion:NULL];

}


- (IBAction)cancelButtonTapped:(id)sender {
    Event *event = [ProtoHelper textInputResponseWithState:TextInputResponseEventStateCancelled text:plainText.text];
    [[IAConnection sharedConnection] sendEvent:event withTag:0];
    [self dismissInputView];
}


- (IBAction)sendButtonTapped:(id)sender {
    [self dismissInputView];
    Event *event = [ProtoHelper textInputResponseWithState:TextInputResponseEventStateEnded text:plainText.text];
    [[IAConnection sharedConnection] sendEvent:event withTag:0];
}

- (IBAction)settingsButtonTapped:(id)sender {
    [self startWifiSetup];
}

- (void)dismissInputView {
    [plainText resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView {
    Event *event = [ProtoHelper textInputResponseWithState:TextInputResponseEventStateChanged text:plainText.text];
    [[IAConnection sharedConnection] sendEvent:event withTag:0];
}

- (IBAction)moreButtonTapped:(id)sender {
    [self toggleControlsView];
}

- (void)toggleControlsView {
    [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:20 options:0
                     animations:^{
                         bottomControlsConstrain.constant = bottomControlsConstrain.constant == 0 ? -54 : 0;
                         [self.view layoutIfNeeded];
                     } completion:NULL];
}

- (IBAction)infoButtonTapped:(id)sender {
    JBWebViewController *infoViewController = [[JBWebViewController alloc] initWithUrl:[NSURL URLWithString:kHelpURL]];
    [infoViewController showFromController:self];
}

- (IBAction)fastForwardButtonTapped:(id)sender {
    [[IAConnection sharedConnection] sendEvent:[ProtoHelper functionEventResponseWithState:FunctionEventKeyMediaFastForward] withTag:0];

}

- (IBAction)playPauseButtonTapped:(id)sender {
    [[IAConnection sharedConnection] sendEvent:[ProtoHelper functionEventResponseWithState:FunctionEventKeyMediaPlay] withTag:0];
}

- (IBAction)rewindButtonTapped:(id)sender {
    [[IAConnection sharedConnection] sendEvent:[ProtoHelper functionEventResponseWithState:FunctionEventKeyMediaRewind] withTag:0];
}


- (IBAction)screenModeButtonTapped:(id)sender {
    [[IAConnection sharedConnection] sendEvent:[ProtoHelper functionEventResponseWithState:FunctionEventKeyF4] withTag:0];
}

- (IBAction)refreshButtonTapped:(id)sender {
    userDidStartScanning = YES;
    [self toggleControlsView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[IAConnection sharedConnection] stop];
        [[IAConnection sharedConnection] resetStates];
        [[IAConnection sharedConnection] start];
    });
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    pageControl.currentPage = page; // you need to have a **iVar** with getter for pageControl
}

- (void)forceStartConnection {
    userDidStartScanning = YES;
    [[IAConnection sharedConnection] stop];
    [[IAConnection sharedConnection] resetStates];
    [[IAConnection sharedConnection] start];
}

- (void)reconnectToServiceIfNeeded {
    if (![IAConnection sharedConnection].isConnected && ![IAConnection sharedConnection].isProcessing) {
        [self forceStartConnection];
    }
}

- (void)dismissControlsBarIfNeeded {
    if (bottomControlsConstrain.constant == 0.0) {
        [self toggleControlsView];
    }
}
@end
