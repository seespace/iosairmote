//
// Created by Long Nguyen on 2/12/15.
// Copyright (c) 2015 Long Nguyen. All rights reserved.
//


#import "WifiSetupController.h"
#import "WSInstructionController.h"
#import "WSNetworksController.h"
#import "WSEnterPasswordController.h"
#import "JDStatusBarNotification+Extension.h"

@implementation WifiSetupController {

}

- (void)viewDidLoad {
  [super viewDidLoad];
  WSInstructionController *initialController = [[WSInstructionController alloc] init];
  [self setViewControllers:@[initialController] animated:NO];

  [self updateNotificationBar];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryStateChanged:) name:UIDeviceBatteryStateDidChangeNotification object:nil];
}

- (void)updateNotificationBar
{
  if ([[EventCenter defaultCenter] isUSBCablePlugged]) {
    if ([[EventCenter defaultCenter] isUSBConnected]) {
      [JDStatusBarNotification showWithStatus:@"Connected to InAiR"];
    } else {
      [JDStatusBarNotification showWithStatus:@"Connecting to InAiR..."];
      [JDStatusBarNotification showActivityIndicator:YES indicatorStyle:UIActivityIndicatorViewStyleGray];
    }
  } else {
    [JDStatusBarNotification showErrorWithStatus:@"USB Cable is not plugged in"];
  }
}

- (void)batteryStateChanged:(NSNotification *)notification
{
  [self updateNotificationBar];
}

- (UIViewController<MBPage> *)setupController:(MBSetupController *)setupController viewControllerAfterViewController:(UIViewController<MBPage> *)viewController {
  if ([viewController isKindOfClass:[WSInstructionController class]]) {
    WSNetworksController *networksController = [[WSNetworksController alloc] init];
    return networksController;
  }
  if ([viewController isKindOfClass:[WSNetworksController class]]) {
    WifiNetwork *network = [(WSNetworksController *) viewController selectedNetwork];
    WSEnterPasswordController *enterPasswordController = [[WSEnterPasswordController alloc] initWithNetwork:network];
    return enterPasswordController;
  }
  return nil;
}

-(void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
