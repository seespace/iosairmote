//
//  WSInstructionController.m
//  Airmote+
//
//  Created by Long Nguyen on 2/12/15.
//  Copyright (c) 2015 Long Nguyen. All rights reserved.
//

#import "WSInstructionController.h"
#import "JDStatusBarNotification+Extension.h"
#import "MBSetupPageSection.h"
#import "MBSectionHeader.h"
#import "MBSectionFooter.h"
#import "IAConnection.h"

@interface WSInstructionController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneCableHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inairCableHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trustDialogHeightConstrain;
@end

@implementation WSInstructionController

- (void)viewDidLoad {
//  [super viewDidLoad];
  // Do any additional setup after loading the view from its nib.

  __weak WSInstructionController *weakSelf = self;
  self.scrollView.contentSize = CGSizeMake(960, self.view.frame.size.height-20);
  weakSelf.nextButtonItem.enabled = [[IAConnection sharedConnection] isUSBConnected];

  if ([IAConnection sharedConnection].isUSBConnected) {
    [self proceedToNextPage];
  }
  BOOL isSmallScreen = fabs([[UIScreen mainScreen] bounds].size.height - 480) < 1;
  if (isSmallScreen) {
    _phoneCableHeightConstrain.constant = 288;
    _inairCableHeightConstrain.constant = 204;
    _trustDialogHeightConstrain.constant = 172;
  }

}

- (void)viewWillAppear:(BOOL)animated {
  [IAConnection sharedConnection].delegate = self;
  [self.navigationController setNavigationBarHidden:true];
}

- (void)viewWillDisappear:(BOOL)animated {
  [self.navigationController setNavigationBarHidden:false];
}

- (void)didStartUSBConnection {
  [self proceedToNextPage];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
  self.pageControl.currentPage = (NSInteger) (scrollView.contentOffset.x / 320.0);
}

// For debugging purpose only
//- (void)didConnect:(NSString *)hostName {
//  [self proceedToNextPage];
//}

- (IBAction)cancel:(id)sender {
  [self dismissViewControllerAnimated:true completion:nil];
  [JDStatusBarNotification dismiss];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
