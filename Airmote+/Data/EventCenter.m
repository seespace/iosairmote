//
// Created by Manh Tuan Cao on 8/21/14.
// Copyright (c) 2014 Long Nguyen. All rights reserved.
//

#import <CocoaAsyncSocket/GCDAsyncSocket.h>
#import <netinet/in.h>
#import <netinet/tcp.h>
#import "EventCenter.h"
#import "Proto.pb.h"
#import "ProtoHelper.h"
#import "Event+Extension.h"
#import "IAConnection.h"

static const int kServicePort = 8989;
static const int kServerSocketPort = 8989;
static const uint8_t kSessionStartTag = 9;
static const uint8_t kPongTag = 11;

@implementation EventCenter
{
  NSNetService *lastConnectedService;
  GCDAsyncSocket *clientSocket;
  GCDAsyncSocket *serverSocket;
  BOOL usbConnected;
}

- (id)init
{
  self = [super init];
  if (self) {
    clientSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    serverSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(usbStateChanged:) name:UIDeviceBatteryStateDidChangeNotification object:nil];
  }

  return self;
}

+ (EventCenter *)defaultCenter
{
  static EventCenter *_instance = nil;

  @synchronized (self) {
    if (_instance == nil) {
      _instance = [[self alloc] init];
    }
  }

  return _instance;
}

- (void)startServer
{
  NSError *error = nil;
  if (![serverSocket acceptOnPort:kServerSocketPort error:&error]) {
    DDLogError(@"Could not listen on port %d. Error: %@", kServicePort, error);
  } else {
    DDLogInfo(@"Listening on port %d", kServicePort);
  }
}

-(BOOL)isUSBCablePlugged {
  return [[UIDevice currentDevice] batteryState] == UIDeviceBatteryStateCharging;
}

- (void)stopServer
{
  DDLogDebug(@"Disconnecting server");
  [serverSocket disconnect];
}


- (BOOL)isActive
{
  return (clientSocket != nil && clientSocket.isConnected);
}

- (BOOL)connectToService:(NSNetService *)netService
{
  NSError *err = nil;

  DDLogDebug(@"Connecting to host: %@", netService);

  if ([netService.addresses count] > 0) {

    if (![clientSocket connectToAddress:netService.addresses[0] withTimeout:10 error:&err]) {

      if ([self.delegate respondsToSelector:@selector(eventCenterFailedToConnectToHost:withError:)]) {
        [self.delegate eventCenterFailedToConnectToHost:netService.hostName withError:err];
      }

      return NO;
    }
  }

  lastConnectedService = netService;
  return YES;
}

- (BOOL)disconnect
{
  DDLogDebug(@"Disconnecting with Client socket");
  clientSocket.delegate = nil;
  [clientSocket disconnect];
  clientSocket.delegate = self;
  return NO;
}

- (void)sendEvent:(Event *)event withTag:(u_int8_t)tag
{
  NSData *data = [Event dataFromEvent:event];
  [clientSocket writeData:data withTimeout:0 tag:tag];

}


- (void)usbStateChanged:(NSNotification *)notification
{
  switch ([[UIDevice currentDevice] batteryState]) {
    case UIDeviceBatteryStateCharging:
      // Do something here?

      break;

    case UIDeviceBatteryStateUnplugged:
      if (!usbConnected) {
        [self usbSocketDidDisconnect];
      }
      break;

    default:
      break;
  }
}

- (void)disconnect:(GCDAsyncSocket *)socket
{
  DDLogDebug(@"Disconnecting with socket: %@", socket);
  socket.delegate = nil;
  [socket disconnect];
  socket.delegate = self;
}
#pragma mark -
#pragma mark Socket methods

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
  NSLog(@"Did read data");

  if (data.length < 4) {
    [self disconnect:sock];
    [self socketDidDisconnect:sock withError:nil];

    return;
  }

  NSData *lengthData = [data subdataWithRange:NSMakeRange(0, 4)];
  int length = CFSwapInt32BigToHost(*(int *) ([lengthData bytes]));
  if (length > data.length) {
    NSLog(@"ERROR: Length value is bigger than actual data length");
    [self disconnect:sock];
    return;
  }

  NSData *msg = [data subdataWithRange:NSMakeRange(4, length)];

  Event *event = [ProtoHelper parseFromData:msg];

  if (event.type == EventTypePing) {
    [self pong];
  } else {
    if (self.delegate && [self.delegate respondsToSelector:@selector(eventCenter:receivedEvent:)]) {
      [self.delegate eventCenter:self receivedEvent:event];
    }
  }

  [sock readDataWithTimeout:-1 tag:0];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)error
{
  DDLogDebug(@"Disconnecting from Socket: %@ ---- %@", sock, error);
  if (sock == clientSocket) {
    [self wifiSocketDidDisconnectWithError:error];
    if (usbConnected) {
      [self usbSocketDidDisconnect];
    } else {
      [self wifiSocketDidDisconnectWithError:error];
    }

  } else if (sock == serverSocket) {
    DDLogDebug(@"Connection with USB is lost");
  }

}

- (void)usbSocketDidDisconnect
{
  usbConnected = NO;
  NSError *err = [NSError errorWithDomain:kIAConnectionErrorDomain code:-1 userInfo:@{NSLocalizedFailureReasonErrorKey : @"USB disconnected"}];
  if (self.delegate && [self.delegate respondsToSelector:@selector(eventCenterDidStopUSBConnectionWithError:)]) {
    [self.delegate eventCenterDidStopUSBConnectionWithError:err];
  }
}

#pragma mark - Server Socket Methods

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
  // Ignore connection user trying to connect using wifi connection
  DDLogDebug(@"Accept new socket....");
  if (sock == serverSocket && (![[IAConnection sharedConnection] isProcessing] || [clientSocket isConnected])) {
    DDLogDebug(@"New socket %@", newSocket);
    [newSocket readDataWithTimeout:-1 tag:0];

    clientSocket = newSocket;
    [clientSocket setDelegate:self delegateQueue:dispatch_get_main_queue()];
    // TCP_NO_DELAY
    [clientSocket performBlock:^{
      int fd = [clientSocket socketFD];
      int on = 1;
      if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *) &on, sizeof(on)) == -1) {
        DDLogDebug(@"Could not set sock opt TCP_NODELAY: %s", strerror(errno));
      }
    }];

    usbConnected = YES;
    [self registerDevice];
    if (self.delegate && [self.delegate respondsToSelector:@selector(eventCenterDidStartUSBConnection)]) {
      [self.delegate eventCenterDidStartUSBConnection];
    }

  } else {
    DDLogError(@"Accepting connection of the Unexpected socket");
  }
}

#pragma mark - Wifi Socket Methods

- (void)wifiSocketDidDisconnectWithError:(NSError *)error
{
  DDLogError(@"Disconnected from socket");
  if (self.delegate && [self.delegate respondsToSelector:@selector(eventCenterDidDisconnectFromHost:withError:)]) {
    [self.delegate eventCenterDidDisconnectFromHost:lastConnectedService.hostName withError:error];
  }
  lastConnectedService = nil;
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
  clientSocket = sock;
  [clientSocket setDelegate:self delegateQueue:dispatch_get_main_queue()];
  [clientSocket performBlock:^{
    int fd = [clientSocket socketFD];
    int on = 1;
    if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *) &on, sizeof(on)) == -1) {
      DDLogDebug(@"Could not set sock opt TCP_NODELAY: %s", strerror(errno));
    }
  }];

  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:host forKey:@"host"];
  [defaults setInteger:port forKey:@"port"];

  // Register this device
  [self registerDevice];

  [clientSocket readDataWithTimeout:-1 tag:0];
  usbConnected = NO;
  if (self.delegate && [self.delegate respondsToSelector:@selector(eventCenterDidConnectToService:)]) {
    [self.delegate eventCenterDidConnectToService:lastConnectedService];
  }
}


#pragma mark - Handshake

- (void)registerDevice
{
  Event *event = [ProtoHelper deviceEventWithTimestamp:[ProtoHelper now] type:DeviceEventTypeRegister];
  DDLogDebug(@"Sending Event: %@", event);
  [self sendEvent:event withTag:kSessionStartTag];
}

- (void)connectToHost:(NSString *)address
{
  NSError *err = nil;

  lastConnectedService = [[NSNetService alloc] initWithDomain:@"tv.inair" type:kManualIPAddress name:address port:0];

  DDLogDebug(@"Connecting to host: %@", address);

  if ([address length] > 0) {

    if (![clientSocket connectToHost:address onPort:kServicePort withTimeout:10 error:&err]) {
      if ([self.delegate respondsToSelector:@selector(eventCenterFailedToConnectToHost:withError:)]) {
        [self.delegate eventCenterFailedToConnectToHost:address withError:err];
      }
    }
  } else {
    if ([self.delegate respondsToSelector:@selector(eventCenterFailedToConnectToHost:withError:)]) {
      [self.delegate eventCenterFailedToConnectToHost:address withError:[NSError errorWithDomain:kIAConnectionErrorDomain
                                                                                            code:IAConnectionErrorFailToConnectSocket
                                                                                        userInfo:@{NSLocalizedFailureReasonErrorKey : @"USB disconnected"}]];
    }
    lastConnectedService = nil;
  }


}

#pragma mark - Pong

- (void)pong
{
  Event *event = [ProtoHelper pongEvent];
  DDLogDebug(@"PONG");
  [self sendEvent:event withTag:kPongTag];
}

- (BOOL)isUSBConnected
{
  return [self isActive] && usbConnected;
}

- (void)dealloc
{
  [[UIDevice currentDevice] setBatteryMonitoringEnabled:NO];
}
@end
