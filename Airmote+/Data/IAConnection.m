//
// Created by Manh Tuan Cao on 11/18/14.
// Copyright (c) 2014 Long Nguyen. All rights reserved.
//

#import "IAConnection.h"
#import "Reachability.h"

#define kServiceType @"_irpc._tcp."
#define kMaxScanningDuration 5.0
#define kMaxResolvingDuration 5.0

@implementation IAConnection
{
  NSNetServiceBrowser *browser;
  NSMutableArray *foundServices;
  NSTimer *timeOutTimer;
  NSNetService *currentService;
  BOOL isConnecting;
  BOOL isResolving;
  BOOL isScanning;
}

#pragma mark - Init


- (EventCenter *)eventCenter
{
  return [EventCenter defaultCenter];
}

- (id)init
{
  self = [super init];
  if (self) {
    browser = [[NSNetServiceBrowser alloc] init];
    foundServices = [[NSMutableArray alloc] init];
    browser.delegate = self;

    self.eventCenter.delegate = self;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterBackground:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
  }
  return self;
}


- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}


+ (IAConnection *)sharedConnection
{
  static IAConnection *_connection = nil;

  @synchronized (self) {
    if (_connection == nil) {
      _connection = [[self alloc] init];
    }
  }

  return _connection;
}

#pragma mark - Getters

- (NSArray *)foundServices
{
  return foundServices;
}

#pragma mark - NetServiceBrowserDelegate

- (void)netServiceBrowserWillSearch:(NSNetServiceBrowser *)aNetServiceBrowser
{
  isScanning = YES;
  DDLogDebug(@"Start Scanning");
  if ([self.delegate respondsToSelector:@selector(didStartScanning)]) {
    [self.delegate didStartScanning];
  }
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aBrowser didFindService:(NSNetService *)aService moreComing:(BOOL)more
{
  DDLogDebug(@"Found services: %@", aService.name);
  [timeOutTimer invalidate];
  [foundServices addObject:aService];

  if (!more) {
    DDLogDebug(@"Found %d services", [foundServices count]);
    isScanning = NO;
    if (foundServices.count > 1) {
      if ([self.delegate respondsToSelector:@selector(didFoundServices:)]) {
        [self.delegate didFoundServices:foundServices];
      }
    } else if (foundServices.count == 1) {
      [self connectToService:foundServices[0]];
    }
  }
}


- (void)netServiceBrowser:(NSNetServiceBrowser *)aBrowser didRemoveService:(NSNetService *)aService moreComing:(BOOL)more
{
  DDLogDebug(@"Removing service: %@", aService.name);
  [foundServices removeObject:aService];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didNotSearch:(NSDictionary *)errorDict
{
  DDLogDebug(@"NetServiceBrowser didNotSearch: %@", errorDict);
  isScanning = NO;
  [timeOutTimer invalidate];
  NSError *error = [self errorWithCode:IAConnectionErrorDidNotSearch withReason:@"Cannot start scanning"];
  [self notifyError:error];
}


- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser
{
  DDLogDebug(@"netServiceBrowserDidStopSearch");
  isScanning = NO;
  [timeOutTimer invalidate];
  if ([foundServices count] == 0) {
    NSError *error = [self errorWithCode:IAConnectionErrorServicesNotFound withReason:@"Devices not found"];
    [self notifyError:error];
  }

}

- (NSError *)errorWithCode:(IAConnectionError)errorCode withReason:(NSString *)reason
{
  return [NSError errorWithDomain:kIAConnectionErrorDomain code:errorCode userInfo:@{NSLocalizedFailureReasonErrorKey : reason}];
}


#pragma mark - App lifecycle

- (void)appWillEnterBackground:(NSNotification *)notification
{
  if ([self.eventCenter isActive]) {
    [self.eventCenter disconnect];
    [browser stop];
  }
}


#pragma mark - Private methods

- (void)startScanningServices
{
  if ([Reachability reachabilityForLocalWiFi].isReachableViaWiFi) {
    DDLogDebug(@"Start scanning for services");
    isScanning = YES;
    [foundServices removeAllObjects];
    [browser searchForServicesOfType:kServiceType inDomain:@"local."];
    timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxScanningDuration
                                                    target:self
                                                  selector:@selector(timerFired:)
                                                  userInfo:nil
                                                   repeats:NO];
  } else {
    if (![self.eventCenter isActive]) {
      // usb not connected
      DDLogDebug(@"Failed to scan because no wifi available");
      NSError *error = [self errorWithCode:IAConnectionErrorWifiNotAvailable withReason:@"Wifi is not available"];
      [self notifyError:error];
    }
  }
}


- (void)notifyError:(NSError *)error
{
  if ([self.delegate respondsToSelector:@selector(didFailToConnect:)]) {
    [self.delegate didFailToConnect:error];
  }
}


- (void)timerFired:(NSTimer *)timer
{
  isScanning = NO;
  if ([self.delegate respondsToSelector:@selector(didFailToConnect:)]) {
    NSError *error = [self errorWithCode:IAConnectionErrorDiscoveryTimedOut withReason:@"Scanning timed out."];
    [self.delegate didFailToConnect:error];
  }
}


#pragma mark - Public methods

- (void)connectToService:(NSNetService *)service
{

  if (isConnecting) {
    DDLogError(@"Ignoring Connecting to service: %@", service.name);
    return;
  }

  DDLogDebug(@"Connecting to service: %@", service.name);

  if ([service.name isEqualToString:currentService.name]) {
    isConnecting = YES;
    [self.eventCenter connectToService:currentService];

  } else {
    isResolving = YES;
    if (currentService) {
      currentService.delegate = nil;
      [currentService stop];
    }
    currentService = service;
    currentService.delegate = self;
    [currentService resolveWithTimeout:kMaxResolvingDuration];

  }

  if ([self.delegate respondsToSelector:@selector(didStartConnecting)]) {
    [self.delegate didStartConnecting];
  }
}


- (BOOL)isProcessing
{
  DDLogDebug(@"Connect: %@ - Scanning: %@ - Resolving: %@",
      isConnecting ? @"YES" : @"NO",
      isScanning ? @"YES" : @"NO",
      isResolving ? @"YES" : @"NO");
  return isConnecting || isScanning || isResolving;
}

- (BOOL)isConnected
{
  return [self.eventCenter isActive];
}

- (void)start
{

  DDLogDebug(@"Starting IAConnection");
  if ([self reconnectToLastConnectedService]) {
    return;
  }

  if (self.isConnected || self.isProcessing) {
    return;
  }

  [self startScanningServices];
}


- (void)stop
{
  DDLogDebug(@"STOP connection");
  if (timeOutTimer != nil) {
    [timeOutTimer invalidate];
    timeOutTimer = nil;
  }
  browser.delegate = nil;
  self.eventCenter.delegate = nil;
  currentService.delegate = nil;

  [self.eventCenter disconnect];
  [currentService stop];

  browser = nil;
  currentService = nil;

  isConnecting = NO;
  isScanning = NO;
  isScanning = NO;
  if ([self.delegate respondsToSelector:@selector(didStopConnection)]) {
    [self.delegate didStopConnection];
  }
}

- (void)connectToHost:(NSString *)ipAddress
{
  isConnecting = YES;
  currentService = [[NSNetService alloc] initWithDomain:@"tv.inair" type:kManualIPAddress name:ipAddress port:8989];
  [self.eventCenter connectToHost:ipAddress];
  if ([self.delegate respondsToSelector:@selector(didStartConnecting)]) {
    [self.delegate didStartConnecting];
  }
}


- (void)resetStates
{
  DDLogDebug(@"Resetting states");
  currentService = nil;

  browser = [[NSNetServiceBrowser alloc] init];
  browser.delegate = self;
  self.eventCenter.delegate = self;
}


- (void)sendEvent:(Event *)event withTag:(u_int8_t)tag
{
  DDLogDebug(@"Sending Event: %@", event);
  if (self.eventCenter.isActive) {
    [self.eventCenter sendEvent:event withTag:tag];
  } else {
    DDLogError(@"Cannot send event, event center is not connected");
    NSString *errorReason = [NSString stringWithFormat:@"Failed to send event:"];
    NSError *error = [self errorWithCode:IAConnectionErrorFailToSendEvent withReason:errorReason];
    [self notifyError:error];
  }

}

- (void)startSocketConnection
{
  [self.eventCenter startServer];
}

- (void)stopSocketConnection
{
  [self.eventCenter stopServer];
}

- (BOOL)isUSBConnected
{
  return [self.eventCenter isUSBConnected];
}


- (void)connectToServiceAtIndex:(NSUInteger)index
{
  if (index < foundServices.count) {
    [self connectToService:foundServices[index]];
  }
}

- (BOOL)reconnectToLastConnectedService
{
  if (currentService == nil)
    return NO;

  if ([self isConnected] || [self isProcessing]) {
    DDLogError(@"Attempting reconnect while is connected or isConnecting...");
    return NO;
  }

  DDLogDebug(@"Reconnecting.....");
  if (currentService.hostName != nil) {
    isConnecting = YES;
    [self.eventCenter connectToService:currentService];
    if ([self.delegate respondsToSelector:@selector(didStartConnecting)]) {
      [self.delegate didStartConnecting];
    }
  } else {
    [self connectToService:currentService];
  }

  return YES;
}


#pragma mark - NetServiceDelegate

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict
{
  DDLogError(@"Service: %@ DID Not resolve", sender.name);
  isResolving = NO;
  NSError *error = [NSError errorWithDomain:kIAConnectionErrorDomain code:IAConnectionErrorServiceNotResolved userInfo:errorDict];
  [self notifyError:error];
  [self invalidateCurrentService];
}

- (void)invalidateCurrentService
{
  DDLogDebug(@"Invalidate current service");
  if (currentService) {
    [foundServices removeObject:currentService];
  }

  currentService = nil;
}


- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
  DDLogDebug(@"Did resolve service: %@ - IP Address: %@", sender.name, sender.addresses);
  isResolving = NO;
  if (currentService == sender) {
    isConnecting = YES;
    [self.eventCenter connectToService:currentService];
    if ([self.delegate respondsToSelector:@selector(didStartConnecting)]) {
      [self.delegate didStartConnecting];
    }
  } else {
    DDLogError(@"ERROR: Trying to connect to another host while connecting to %@", currentService.hostName);
  }
}

- (void)eventCenterDidConnectToService:(NSNetService *)netService
{
  DDLogDebug(@"Did connect to host name %@", netService);
  isConnecting = NO;
  if ([netService.hostName isEqualToString:currentService.hostName] || [netService.type isEqualToString:kManualIPAddress]) {
    if ([self.delegate respondsToSelector:@selector(didConnect:)]) {
      [self.delegate didConnect:currentService.name];
    }
  }
}

- (void)eventCenterDidDisconnectFromHost:(NSString *)hostName withError:(NSError *)err
{
  DDLogError(@"Event did disconnect from host: %@ - Error %@", hostName, err);
  isConnecting = NO;
  NSError *error = [NSError errorWithDomain:kIAConnectionErrorDomain code:IAConnectionErrorSocketDisconnected userInfo:[err userInfo]];
  [self notifyError:error];
}

- (void)eventCenter:(EventCenter *)eventCenter1 receivedEvent:(Event *)event
{
  DDLogDebug(@"Event center did received: %@", event);
  if ([self.delegate respondsToSelector:@selector(didReceiveEvent:)]) {
    [self.delegate didReceiveEvent:event];
  }
}

- (void)eventCenterFailedToConnectToHost:(NSString *)hostName withError:(NSError *)error
{
  DDLogDebug(@"Failed to connect to host: %@ - error: %@", hostName, error);
  isConnecting = NO;
  NSError *tmpError = [NSError errorWithDomain:kIAConnectionErrorDomain
                                          code:IAConnectionErrorFailToConnectSocket
                                      userInfo:[error userInfo]];
  [self notifyError:tmpError];
  [self invalidateCurrentService];
}

#pragma mark USB Connection

- (void)eventCenterDidStartUSBConnection
{
  if ([self.delegate respondsToSelector:@selector(didStartUSBConnection)]) {
    [self.delegate didStartUSBConnection];
  }
}

- (void)eventCenterDidStopUSBConnectionWithError:(NSError *)error
{
  if (currentService == nil) {
    if ([self.delegate respondsToSelector:@selector(didStopUSBConnection:)]) {
      [self.delegate didStopUSBConnection:error];
    }
  } else {
    [self reconnectToLastConnectedService];
  }

}

@end
